// @flow

import React, { Component } from "react";
import { Note } from "@contentful/forma-36-react-components";
import { init, locations } from "contentful-ui-extensions-sdk";
import "@contentful/forma-36-react-components/dist/styles.css";
import "@contentful/forma-36-fcss/dist/styles.css";
import { getField, isCompatibleImageField } from "../../utils";
import ReactDOM from "react-dom";
import FocalPointView from "../FocalPointView/FocalPointView";
import FocalPointDialog from "../FocalPointDialog/FocalPointDialog";

type Props = {
    sdk: *,
};

type State = {
    value: *,
};

const IMAGE_FIELD_ID = "imageUrl";

/**
 * Extension
 */
export default class Extension extends Component<Props, State> {
    state = {
        value: this.props.sdk.field?.getValue() || null,
    };

    detachExternalChangeHandler = null;
    /**
     * Component Mount => Setup Contentful UI extension
     */
    componentDidMount() {
        const { sdk } = this.props;
        sdk.window.startAutoResizer();

        // Handler for external field value changes (e.g. when multiple authors are working on the same entry).
        this.detachExternalChangeHandler = sdk.field?.onValueChanged(
            this.onExternalChange,
        );
    }

    componentWillUnmount() {
        if (this.detachExternalChangeHandler) {
            this.detachExternalChangeHandler();
        }
    }

    onExternalChange = (value: *) => {
        if (value) {
            this.setState({ value });
        }
    };

    findProperLocale() {
        if (
            this.props.sdk.entry.fields[this.props.sdk.field.id].type === "Link"
        ) {
            return this.props.sdk.locales.default;
        }

        return this.props.sdk.field.locale;
    }

    setFocalPoint = (focalPoint: *) => {
        this.setState(
            {
                value: focalPoint,
            },
            () => this.props.sdk.field.setValue(this.state.value),
        );
    };

    showFocalPointDialog = async () => {
        const {
            sdk: { notifier, space, entry },
        } = this.props;

        try {
            const imageField = entry.fields[IMAGE_FIELD_ID];
            const asset = await space.getAsset(imageField.getValue().sys.id);
            const file = asset.fields.file[this.findProperLocale()];
            const imageUrl = file.url;
            const isOfImageMimeType = /image\/.*/.test(file.contentType);

            if (!isOfImageMimeType) {
                notifier.error("The uploaded asset must be an image");
                return;
            }

            if (!imageUrl) {
                notifier.error("Add an image to the entry first");
                return;
            }

            const focalPoint = await this.props.sdk.dialogs.openExtension({
                width: 1000,
                parameters: {
                    file,
                    focalPoint: this.state.value,
                },
            });

            if (focalPoint) {
                this.setFocalPoint(focalPoint);
            }
        } catch (e) {
            notifier.error("Add an image to the entry first");
            return;
        }
    };

    /**
     * Render => Contentful UI Extension
     */
    render() {
        const { sdk } = this.props;
        const imageField = getField(sdk.contentType, IMAGE_FIELD_ID);
        const isImageField = isCompatibleImageField(imageField);

        if (isImageField) {
            return (
                <FocalPointView
                    showFocalPointDialog={this.showFocalPointDialog}
                    focalPoint={this.state.value}
                />
            );
        }

        return (
            <Note noteType="negative">
                Could not find a field of type Asset with the ID &quot;
                {IMAGE_FIELD_ID}&quot;
            </Note>
        );
    }
}

function renderDialog(sdk) {
    const { invocation: otherProps } = sdk.parameters;
    const container = document.createElement("div");
    const CONTAINER_ID = "focal-point-dialog";

    container.id = CONTAINER_ID;

    if (document.body) {
        document.body.appendChild(container);

        sdk.window.startAutoResizer();

        const containerEl = document.getElementById(CONTAINER_ID);
        if (containerEl) {
            ReactDOM.render(
                <FocalPointDialog
                    sdk={sdk}
                    onSave={data => {
                        sdk.close(data);
                    }}
                    onClose={() => sdk.close()}
                    {...otherProps}
                />,
                containerEl,
            );
        }
    }
}

init(sdk => {
    const root = document.getElementById("root");
    if (root !== null) {
        if (sdk.location.is(locations.LOCATION_DIALOG)) {
            renderDialog(sdk);
        }
    }
});
