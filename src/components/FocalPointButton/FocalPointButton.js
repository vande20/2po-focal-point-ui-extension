// @flow
import React from "react";
import "./style.scss";
import { useState, useEffect } from "react";

const FocalPointButton = (props: *) => {
    const [selected, setSelected] = useState(false);

    useEffect(() => {
        if (props.activeIndex === props.index) {
            setSelected(!selected);
        } else {
            setSelected(false);
        }
    }, [props.activeIndex]);

    const handleFocalPoint = (e: *) => {
        props.getFocalPoint(e, props.index);
    };

    return (
        <div
            key={props.key}
            className={`${props.className} ${selected ? "selected" : ""}`}
            title={props.title}
            onClick={e => handleFocalPoint(e)}
        ></div>
    );
};

export default FocalPointButton;
