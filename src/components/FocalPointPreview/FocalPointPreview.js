//@flow
import React, { useEffect, useState } from "react";
import "./style.scss";

const getZoomFactor = (imgHeight, wrapperHeight) => {
    return Math.max(1, wrapperHeight / imgHeight);
};

const FocalPointPreview = (props: *) => {
    const [coords, setCoords] = useState(null);
    const [imgDimensions, setImgDimensions] = useState({ width: 0, height: 0 });

    useEffect(() => {
        calculateDimensions();
        generateCoords(imgDimensions.height, imgDimensions.width);
    }, []);

    useEffect(() => {
        generateCoords(imgDimensions.height, imgDimensions.width);
    }, [imgDimensions]);

    useEffect(() => {
        generateCoords(imgDimensions.height, imgDimensions.width);
    }, [props.focalPoint]);

    const calculateDimensions = () => {
        let sizingRatio =
            props.imageSrc.details.image.width / props.wrapperWidth;
        let baseImgHeight = props.imageSrc.details.image.height / sizingRatio;

        let wrapperHeight = props.wrapperHeight || baseImgHeight;
        let zoom = getZoomFactor(baseImgHeight, wrapperHeight);
        setImgDimensions({
            width: Math.round(props.wrapperWidth * zoom),
            height: Math.round(baseImgHeight * zoom),
        });
    };

    const generateCoords = (height, width) => {
        let heightDelta =
            height / props.wrapperHeight > 1
                ? (height - props.wrapperHeight) / 2
                : 0;
        let widthDelta =
            width / props.wrapperWidth > 1
                ? (width - props.wrapperWidth) / 2
                : 0;

        setCoords({
            top_left: { x: 0, y: 0 },
            top: { x: -widthDelta, y: 0 },
            top_right: { x: -widthDelta * 2, y: 0 },
            left: { x: 0, y: -heightDelta },
            center: { x: -widthDelta, y: -heightDelta },
            right: { x: -widthDelta * 2, y: -heightDelta },
            bottom_left: { x: 0, y: -heightDelta * 2 },
            bottom: { x: -widthDelta, y: -heightDelta * 2 },
            bottom_right: { x: -widthDelta * 2, y: -heightDelta * 2 },
        });
    };

    return (
        <>
            <div
                className={`preview-container-dimension`}
                style={{
                    width: `${props.wrapperWidth}px`,
                    height: `${props.wrapperHeight}px`,
                }}
            >
                {coords && (
                    <img
                        src={props.imageSrc.url}
                        style={{
                            transform: `translate3d(${coords[props.focalPoint]?.x}px, ${coords[props.focalPoint]?.y}px, 0)`,
                            width: `${imgDimensions.width}px`,
                            height: `${imgDimensions.height}px`,
                        }}
                    />
                )}
            </div>
        </>
    );
};

export default FocalPointPreview;
