// @flow
import React, { Component } from "react";
import "./style.scss";
import {
    Button,
    Subheading,
    Modal,
} from "@contentful/forma-36-react-components";
import FocalPointButton from "../FocalPointButton/FocalPointButton";
import FocalPointPreview from "../FocalPointPreview/FocalPointPreview";

type Props = {
    file: string,
    onClose: *,
    onSave: *,
    focalPoint: string,
};

type State = {
    focalPoint: string,
};

class FocalPointDialog extends Component<Props, State> {
    state = {
        focalPoint: this.props.focalPoint || "center",
    };

    getFocalPoint = e => {
        this.setState({ focalPoint: e.target.title });
    };

    render() {
        const { file } = this.props;
        const positions = [
            "top_left",
            "top",
            "top_right",
            "left",
            "center",
            "right",
            "bottom_left",
            "bottom",
            "bottom_right",
        ];

        const wrapperStyle =
            file.details.image.width / file.details.image.height > 1
                ? "w-100"
                : "h-100";

        const overlayButtons = positions.map(pos => (
            <FocalPointButton
                key={pos}
                index={pos}
                activeIndex={this.state.focalPoint}
                className={"focal-overlay-button"}
                title={pos}
                getFocalPoint={this.getFocalPoint}
            />
        ));

        return (
            <>
                <Modal.Header title="Set focal point" />
                <Modal.Content className="modal-content">
                    <div className="focal-point-grid">
                        <Subheading>Select position of focal point</Subheading>
                        <p>
                            Selected position:{" "}
                            <span className="focal-point-position">
                                {this.state.focalPoint.replace("_", " - ")}
                            </span>
                        </p>
                        <div className={"image-container"}>
                            <div
                                className={`focal-point-wrapper ${wrapperStyle}`}
                            >
                                <img src={file.url} alt="focal point preview" />
                                <div className={"focal-overlay"}>
                                    {overlayButtons}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="focal-point-preview">
                        <h4>Preview for different screen sizes</h4>
                        <div className="preview-container">
                            <FocalPointPreview
                                imageSrc={file}
                                screen={"desktop"}
                                wrapperWidth={420}
                                wrapperHeight={180}
                                focalPoint={this.state.focalPoint}
                            />
                        </div>
                        <div className="preview-container">
                            <FocalPointPreview
                                imageSrc={file}
                                screen={"tablet"}
                                wrapperWidth={280}
                                wrapperHeight={180}
                                focalPoint={this.state.focalPoint}
                            />
                            <FocalPointPreview
                                imageSrc={file}
                                screen={"mobile"}
                                wrapperWidth={120}
                                wrapperHeight={180}
                                focalPoint={this.state.focalPoint}
                            />
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Controls>
                    <Button
                        onClick={() => this.props.onSave(this.state.focalPoint)}
                        buttonType="positive"
                    >
                        Save
                    </Button>
                    <Button onClick={this.props.onClose} buttonType="muted">
                        Close
                    </Button>
                </Modal.Controls>
            </>
        );
    }
}

export default FocalPointDialog;
