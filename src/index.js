import React from "react";
import ReactDOM from "react-dom";
import { init as initContentfulExtension } from "contentful-ui-extensions-sdk";
import { locations } from "contentful-ui-extensions-sdk";
import { Extension } from "./components";
import "./index.css";

/**
 * initialise a contentful extension and parse the sdk
 */
initContentfulExtension(sdk => {
    if (!sdk.location.is(locations.LOCATION_DIALOG)) {
        ReactDOM.render(
            <Extension sdk={sdk} />,
            document.getElementById("root"),
        );
    }
});

if (module.hot) {
    module.hot.accept();
}
