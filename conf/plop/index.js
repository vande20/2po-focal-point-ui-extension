// @flow

import component from "./component";

export default (plop: *) => {
    // Component generator
    component(plop);
};
